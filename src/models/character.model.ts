export interface Character {
  id: number;
  name: string;
  status: 'Dead'|'Alive'|'unknown';
  species: string;
  type: string;
  gender: string;
  origin: Origin;
  location: Location;
  image: string;
  episode: string[];
  created: string;
}

interface GenericType {
  name: string;
  url: string;
}

interface Origin extends GenericType {
}

interface Location extends GenericType {

}