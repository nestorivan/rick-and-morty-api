import React from 'react';
import { render as rtlRender, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import {rest} from 'msw';
import {setupServer} from 'msw/node';

import App from './App';
import { configureStore } from '@reduxjs/toolkit';
import charactersReducer from './store/reducers/characters.reducer';
import episodesReducer from './store/reducers/episodes.reducer';

const mockedCharacter = {
  "id": 1,
  "name": "Rick Sanchez",
  "status": "Alive",
  "species": "Human",
  "type": "",
  "gender": "Male",
  "origin": {
    "name": "Earth",
    "url": "https://rickandmortyapi.com/api/location/1"
  },
  "location": {
    "name": "Earth",
    "url": "https://rickandmortyapi.com/api/location/20"
  },
  "image": "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
  "episode": [
    "https://rickandmortyapi.com/api/episode/1",
  ],
  "url": "https://rickandmortyapi.com/api/character/1",
  "created": "2017-11-04T18:48:46.250Z"
}

const mockedEpisode = {
  "id": 1,
  "name": "Pilot",
  "air_date": "December 2, 2013",
  "episode": "S01E01",
  "characters": [
    "https://rickandmortyapi.com/api/character/1",
    "https://rickandmortyapi.com/api/character/2",
  ],
  "url": "https://rickandmortyapi.com/api/episode/1",
  "created": "2017-11-10T12:56:33.798Z"
}

// const preloadedState : RootState = {
//   characters: {
//     loading: false,
//     error: false,
//     errorMsg: '',
//     characters: []
//   },
//   episodes: {
//     loading: false,
//     error: false,
//     errorMsg: '',
//     episodes: []
//   }
// };

const render=((
  ui: JSX.Element,
  {
    preloadedState,
    store = configureStore({ reducer: { characters: charactersReducer, episodes: episodesReducer }, preloadedState }),
    ...renderOptions
  }: {
    preloadedState?: any,
    store?: any
  } = {}
) => {
  function Wrapper({ children }: {children:any}) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
});

export const handlers = [
  rest.get(`https://rickandmortyapi.com/api/character`,(req,res,ctx)=> {
    return res(
      ctx.json({
        info:{},
        results: [mockedCharacter]
      }), 
      ctx.delay(50)
    )
  }),
  rest.get(`https://rickandmortyapi.com/api/episode/1`, (req,res,ctx)=> {
    return res(
      ctx.json({
        info:{},
        results: [mockedEpisode]
      }), 
      ctx.delay(50)
    )}
  )
]

const server = setupServer(...handlers)

// Enable API mocking before tests.
beforeAll(() => server.listen())

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())

// Disable API mocking after the tests are done.
afterAll(() => server.close())

test('loads and renders character list', async() => {
  render(<App />);
  
  expect(screen.getByText(/loading/i)).toBeInTheDocument();
  
  await waitFor(() => {
    expect(screen.getByText(/rick sanchez/i)).toBeInTheDocument()
  });
})