import axios from "axios"
import { StatusIndicatorProps } from "../components/character/character-info"
import { Character } from "../models/character.model"
import { Info } from "../models/info.model"

const characterEndpoint = `https://rickandmortyapi.com/api/character`

export interface Pagination{
  count?: number;
  page?: number;
  next?: string;
  prev?: string;
}

export interface EndpointOptions extends Pagination{
  name?: string;
  status?: StatusIndicatorProps;
  species?: string;
  type?: string;
  gender?: 'female'| 'male'| 'genderless' | 'unknown'
}

interface EndpointResponse{
  data: {
    info:Info;
    results: Character[];
  }
}

export const getCharacterList = (options?: EndpointOptions): Promise<EndpointResponse> => axios.get(characterEndpoint, {
  params: {
    ...options
  }  
})