import axios from "axios";
import { Episode } from "../models/episode.model";
import { Info } from "../models/info.model";
import { Pagination } from "./character.service";

const characterEndpoint = `https://rickandmortyapi.com/api/episode/`


export interface EpisodeEndpointOptions extends Pagination{
  name?: string;
  episode?: string[];
}

interface EndpointResponse{
  data:Episode[];
}

const getEpisodesById = (options?: EpisodeEndpointOptions): Promise<EndpointResponse> => axios.get(`${characterEndpoint}${options?.episode}`);

export default getEpisodesById;