import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Character from './components/character/character';
import {  getCharacters } from './store/reducers/characters.reducer';
import { RootState } from './store/store';

const App = () => {

  const dispatch = useDispatch();
  const {loading, error, errorMsg, characters } = useSelector((state:RootState) => state.characters)

  useEffect(()=>{
    dispatch(getCharacters({}));
  }, []);

if(loading)
    return (<span>loading</span>)
  
  if(error)
    return (<span>{{errorMsg}}</span>)

  return (
    <div className="App">
      {
        characters?.length > 0 ? characters.map(c => <Character key={c.id} character={c} />) : <span>no results</span>
      }
    </div>
  );
}

export default App;
