import { combineReducers } from '@reduxjs/toolkit';
import charactersReducer from './characters.reducer'
import episodesReducer from './episodes.reducer';

const rootReducer = combineReducers({ characters: charactersReducer, episodes: episodesReducer })

export default rootReducer;