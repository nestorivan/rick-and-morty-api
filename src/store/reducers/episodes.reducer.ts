import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Episode } from "../../models/episode.model";
import getEpisodesById from "../../service/episodes.service";

interface EpisodesReducerStore{
  loading: boolean;
  error: boolean;
  errorMsg: string | undefined;
  episodes: Episode[];
}

const initialState: EpisodesReducerStore = {
  loading: false,
  error: false,
  errorMsg: '', 
  episodes: []
}

export const getEpisodes = createAsyncThunk('episodes/fetchEpisodesList', 
  async(episodesList: string[]) => {
    const {data: episodes} = await getEpisodesById({episode: Array.from(episodesList)})
    return episodes
})

const episodesSlice = createSlice({
  name: 'episodes',
  initialState: initialState, 
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getEpisodes.pending, (state) => {
      state.loading = true;
      state.error = false;
      state.errorMsg = ''
    })
    builder.addCase(getEpisodes.rejected, (state, action) => {
      state.loading = false;
      state.error = true;
      state.errorMsg = action.error.message;
    })
    builder.addCase(getEpisodes.fulfilled, (state, action) => {
      state.loading = false;
      state.episodes = action.payload;
    })
  }
})


export default episodesSlice.reducer;
