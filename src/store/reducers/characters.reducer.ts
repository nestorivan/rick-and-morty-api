import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Character } from "../../models/character.model";
import { EndpointOptions, getCharacterList } from "../../service/character.service";
import { getEpisodes } from "./episodes.reducer";

interface charactersReducerStore {
    loading: boolean;
    error: boolean;
    errorMsg: string | undefined;
    characters: Character[]
}

const initialState: charactersReducerStore = {
  loading: false,
  error: false,
  errorMsg: '',
  characters: []
}

enum actionType {
    LOADING,
    ERROR,
    SET_CHARACTERS
}

export const getCharacters = createAsyncThunk('character/fetchCharacterList', 
  async(options?: EndpointOptions, thunkApi?) => {
    const {data: {results}} = await getCharacterList(options);
    
    const episodesList = results.reduce((acc,c)=> {
      const episodes = c.episode.map(e => e.split('/').pop());
      acc.push(...episodes)
      return acc;
    }, ([] as any))

    const filteredEpisodesList = Array.from(new Set([...episodesList]));

    thunkApi.dispatch(getEpisodes(filteredEpisodesList))

    return results;
})

const charactersSlice = createSlice({
    name: 'characters',
    initialState: initialState, 
    reducers: {},
    extraReducers: builder => {
      builder.addCase(getCharacters.pending, (state) => {
        state.loading = true;
        state.error = false;
        state.errorMsg = '';
      })
      builder.addCase(getCharacters.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
        state.errorMsg = action.error.message;
      })
      builder.addCase(getCharacters.fulfilled, (state, action) => {
        state.loading = false;
        state.characters = action.payload;
      })
    }
})


export default charactersSlice.reducer;
