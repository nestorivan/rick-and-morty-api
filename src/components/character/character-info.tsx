import styled from "styled-components";
import { Character } from "../../models/character.model";
import { Episode } from "../../models/episode.model";

interface Props {
  character: Character, 
  episode?: Episode
}

export interface StatusIndicatorProps{
  status: 'Dead'|'Alive'|'unknown'
}

const statusColors = {
  'Dead': 'rgb(214, 61, 46)',
  'Alive': 'rgb(85, 204, 68)',
  'unknown': '#dedede',
}

const CharacterInfoContainer = styled.div`
  flex: 3 0 0%;
  color: white;
  padding: 0.75rem;
  flex-direction:column;

  .title{
    margin:0;
  }

  &section.section{
    flex: 1 1 0%;
    display: flex;
    flex-direction: column;
  }

`

const Status = styled.div`
  display: flex;
  align-items: center;
`

const StatusIndicator = styled.span`
  border-radius: 50%;
  height: 0.5rem;
  width: 0.5rem;
  background: ${({status}: StatusIndicatorProps) => status ? statusColors[status] : '' };
  margin: 0.375rem;
`

const CharacterInfo: React.FC<Props> = ({character, episode}) => {
  return (
    <CharacterInfoContainer>
      <section className="section">
        <h2 className="title">{character?.name}</h2>
        <Status>
          <StatusIndicator status={character?.status} />
          {character?.status} - {character?.species}
        </Status>
      </section>
      <section className="section">
        <span>Last known location: {character?.location?.name}</span>
      </section>
      <section className="section">
        <span>
          First seen in: {episode?.name}
        </span>
      </section>

    </CharacterInfoContainer>
  )
}

export default CharacterInfo;