import { useSelector } from "react-redux";
import styled from "styled-components";
import { Character as CharacterModel } from "../../models/character.model";
import { RootState } from "../../store/store";
import CharacterInfo from "./character-info";
import CharacterPortrait from "./character-portrait";

interface Props{
  character: CharacterModel
}

const CharacterCard = styled.div`
  display: flex;
  width: 600px;
  height: 220px;
  overflow: hidden;
  background: rgb(60, 62, 68) none repeat scroll 0% 0%;
  border-radius: .5rem;
  margin:.75rem;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
`

const Character: React.FC<Props> = ({character}) => {

    const { episodes } = useSelector((state: RootState) => state.episodes)

    const characterFirstEpisode = episodes?.find(e => e.id === parseInt(character.episode[0].split('/').pop() as string))

    return (
        <CharacterCard>
          <CharacterPortrait img={character.image} />
          <CharacterInfo character={character} episode={characterFirstEpisode} ></CharacterInfo>
        </CharacterCard>
    )
}

export default Character;