import styled from "styled-components";

interface Props{
  img: string;
}

const CharacterPortraitContainer = styled.div`
  flex: 2 1 0%;
  width: 100%
`

const Portrait = styled.img`
  width: 100%
`

const CharacterPortrait:React.FC<Props> = ({img}) => {
  return (
    <CharacterPortraitContainer>
      <Portrait src={img} />
    </CharacterPortraitContainer>
  )
}

export default CharacterPortrait;